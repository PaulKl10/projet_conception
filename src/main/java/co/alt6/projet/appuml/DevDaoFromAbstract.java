package co.alt6.projet.appuml;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import co.alt6.projet.appuml.entity.Developpeur;
import co.alt6.projet.appuml.interfaces.DevDao;

public class DevDaoFromAbstract extends AbstractDao<Developpeur> implements DevDao {

    public DevDaoFromAbstract() {
        super(
                "INSERT INTO developpeur (firstname,lastname,email,password,country,tarif) VALUES (?,?,?,?,?,?)",
                "SELECT * FROM developpeur",
                "SELECT * FROM developpeur WHERE id=?",
                "UPDATE developpeur SET firstname=?,lastname=?,email=?, password=?, country=?, tarif=? WHERE id=?",
                "DELETE FROM developpeur WHERE id=?");
    }

    @Override
    protected Developpeur sqlToEntity(ResultSet rs) throws SQLException {
        return new Developpeur(
                rs.getInt("id"),
                rs.getString("firstname"),
                rs.getString("lastname"),
                rs.getString("email"),
                rs.getString("password"),
                rs.getString("country"),
                rs.getDouble("tarif"));
    }

    @Override
    protected void setEntityId(Developpeur entity, int id) {
        entity.setId(id);
    }

    @Override
    protected void entityBindValues(PreparedStatement stmt, Developpeur entity) throws SQLException {
        stmt.setString(1, entity.getFirstname());
        stmt.setString(2, entity.getLastname());
        stmt.setString(3, entity.getEmail());
        stmt.setString(4, entity.getPassword());
        stmt.setString(5, entity.getCountry());
        stmt.setDouble(6, entity.getTarif());
    }

    @Override
    protected void entityBindValuesWithId(PreparedStatement stmt, Developpeur entity) throws SQLException {
        entityBindValues(stmt, entity);
        stmt.setInt(7, entity.getId());
    }

}
