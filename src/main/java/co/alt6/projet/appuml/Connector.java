package co.alt6.projet.appuml;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import co.alt6.projet.appuml.util.AppProperties;

public class Connector {

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(AppProperties.getInstance().getProp("DATABASE_URL"));
    }
}
