package co.alt6.projet.appuml;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractDao<E> {

    protected String addSQL;
    protected String getAllSQL;
    protected String getByIdSQL;
    protected String updateSQL;
    protected String deleteSQL;

    public AbstractDao(String addSQL, String getAllSQL, String getByIdSQL, String updateSQL, String deleteSQL) {
        this.addSQL = addSQL;
        this.getAllSQL = getAllSQL;
        this.getByIdSQL = getByIdSQL;
        this.updateSQL = updateSQL;
        this.deleteSQL = deleteSQL;
    }

    public List<E> getAll() {
        List<E> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getAllSQL);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                E entity = sqlToEntity(rs);
                list.add(entity);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public E getById(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(getByIdSQL);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                E entity = sqlToEntity(rs);
                return entity;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean delete(int id) {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(deleteSQL);
            stmt.setInt(1, id);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean add(E entity) {

        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(addSQL, Statement.RETURN_GENERATED_KEYS);
            entityBindValues(stmt, entity);
            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            rs.next();
            setEntityId(entity, rs.getInt(1));

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean update(E entity) {

        try (Connection connection = Connector.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement(updateSQL);
            entityBindValuesWithId(stmt, entity);

            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    protected abstract void setEntityId(E entity, int id);

    protected abstract void entityBindValues(PreparedStatement stmt, E entity) throws SQLException;

    protected abstract void entityBindValuesWithId(PreparedStatement stmt, E entity) throws SQLException;

    protected abstract E sqlToEntity(ResultSet rs) throws SQLException;

}
