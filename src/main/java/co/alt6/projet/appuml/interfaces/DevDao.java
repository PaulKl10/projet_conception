package co.alt6.projet.appuml.interfaces;

import co.alt6.projet.appuml.entity.Developpeur;

public interface DevDao extends Dao<Developpeur> {
}
