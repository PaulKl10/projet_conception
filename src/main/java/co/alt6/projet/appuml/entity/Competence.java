package co.alt6.projet.appuml.entity;

public class Competence {
    private int id;
    private String name;

    public Competence(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
