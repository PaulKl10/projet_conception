package co.alt6.projet.appuml.entity;

import java.util.ArrayList;
import java.util.List;

public class PorteurDeProjet extends Client {

    private List<Message> messages = new ArrayList<>();
    private List<Avis> avis = new ArrayList<>();
    private List<Transaction> transactions = new ArrayList<>();
    private List<Projet> projets = new ArrayList<>();

    public PorteurDeProjet(int id, String firstname, String lastname, String email, String password, String country) {
        super(id, firstname, lastname, email, password, country);
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message message) {
        this.messages.add(message);
    }

    public List<Avis> getAvis() {
        return avis;
    }

    public void setAvis(List<Avis> avis) {
        this.avis = avis;
    }

    public void addAvis(Avis avis) {
        this.avis.add(avis);
    }

    public List<Transaction> getTransaction() {
        return transactions;
    }

    public void setTransaction(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void addTransaction(Transaction transaction) {
        this.transactions.add(transaction);
    }

    public List<Projet> getProjets() {
        return projets;
    }

    public void setProjets(List<Projet> projets) {
        this.projets = projets;
    }

    public void addProjet(Projet projet) {
        this.projets.add(projet);
    }

}
