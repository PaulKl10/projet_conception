package co.alt6.projet.appuml.entity;

import java.sql.Date;

public class Devis {
    private int id;
    private String titre;
    private double tarif;
    private String texte;
    private Date date;
    private boolean status;
    private Projet projet;

    public Devis(String titre, double tarif, String texte, Date date, boolean status) {
        this.titre = titre;
        this.tarif = tarif;
        this.texte = texte;
        this.date = date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }

    public String getTexte() {
        return texte;
    }

    public void setTexte(String texte) {
        this.texte = texte;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }
}
