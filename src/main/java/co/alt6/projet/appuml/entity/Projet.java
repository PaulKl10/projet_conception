package co.alt6.projet.appuml.entity;

import java.util.List;

public class Projet {
    private int id;
    private String titre;
    private String description;
    private String image;
    private String category;
    private double prix;
    private List<Devis> devis;

    public Projet(String titre, String description, String image, String category, double prix) {
        this.titre = titre;
        this.description = description;
        this.image = image;
        this.category = category;
        this.prix = prix;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public List<Devis> getDevis() {
        return devis;
    }

    public void setDevis(List<Devis> devis) {
        this.devis = devis;
    }

    public void addDevis(Devis devis) {
        this.devis.add(devis);
    }

}
