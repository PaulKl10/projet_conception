package co.alt6.projet.appuml.entity;

import java.sql.Date;
import java.util.List;

public class Transaction {
    private Date date;
    private List<Devis> devis;

    public Transaction(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<Devis> getDevis() {
        return devis;
    }

    public void setDevis(List<Devis> devis) {
        this.devis = devis;
    }

    public void addDevis(Devis devis) {
        this.devis.add(devis);
    }

}
