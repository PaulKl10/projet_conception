package co.alt6.projet.appuml.entity;

import java.util.ArrayList;
import java.util.List;

public class Developpeur extends Client {

    private double tarif;
    private List<Competence> competences = new ArrayList<>();
    private List<Experience> experiences = new ArrayList<>();
    private List<Message> messages = new ArrayList<>();
    private List<Avis> avis = new ArrayList<>();
    private List<Devis> devis = new ArrayList<>();
    private List<Projet> projets = new ArrayList<>();

    public Developpeur(int id, String firstname, String lastname, String email, String password, String country,
            double tarif) {
        super(id, firstname, lastname, email, password, country);
        this.tarif = tarif;
    }

    public double getTarif() {
        return tarif;
    }

    public void setTarif(double tarif) {
        this.tarif = tarif;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public void setCompetences(List<Competence> competences) {
        this.competences = competences;
    }

    public void addCompetence(Competence competences) {
        this.competences.add(competences);
    }

    public List<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(List<Experience> experiences) {
        this.experiences = experiences;
    }

    public void addExperience(Experience experience) {
        this.experiences.add(experience);
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public void addMessage(Message message) {
        this.messages.add(message);
    }

    public List<Avis> getAvis() {
        return avis;
    }

    public void setAvis(List<Avis> avis) {
        this.avis = avis;
    }

    public void addAvis(Avis avis) {
        this.avis.add(avis);
    }

    public List<Devis> getDevis() {
        return devis;
    }

    public void setDevis(List<Devis> devis) {
        this.devis = devis;
    }

    public void addDevis(Devis devis) {
        this.devis.add(devis);
    }

    public List<Projet> getProjets() {
        return projets;
    }

    public void setProjets(List<Projet> projets) {
        this.projets = projets;
    }

    public void addProjet(Projet projet) {
        this.projets.add(projet);
    }

}
