package co.alt6.projet.appuml.entity;

public class Avis {
    private int id;
    private String avis;

    public Avis(String avis) {
        this.avis = avis;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAvis() {
        return avis;
    }

    public void setAvis(String avis) {
        this.avis = avis;
    }

}
