package co.alt6.projet.appuml.util;

import java.io.IOException;
import java.util.Properties;

public class AppProperties {

    private static AppProperties instance;
    private Properties props;

    private AppProperties() {
        this.props = new Properties();
        try {
            this.props.load(AppProperties.class.getResourceAsStream("/app.properties"));
        } catch (IOException e) {
            System.out.println("App properties load error");
            e.printStackTrace();
        }
    }

    public static AppProperties getInstance() {
        if (instance == null) {
            instance = new AppProperties();
        }
        return instance;
    }

    public String getProp(String key) {
        return this.props.getProperty(key);
    }
}
